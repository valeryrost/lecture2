package sbp.branching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.common.Utils;

import static org.mockito.ArgumentMatchers.anyString;

public class MyBranchingTest {

    //Проверка результата - должен быть 0 при Utils#utilFunc2 true;
    @Test
    public void maxInt_Test_True()
    {
        int iMin = 1;
        int iMax = 2;
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        MyBranching myBranching = new MyBranching(utilsMock);
        int result = myBranching.maxInt(iMin,iMax);
        Assertions.assertEquals(0,result);
    }

    //Проверка результата - должено быть макс. при Utils#utilFunc2 false
    @Test
    public void maxInt_Test_False()
    {
        int iMin = 1;
        int iMax = 2;
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        MyBranching myBranching = new MyBranching(utilsMock);
        int result = myBranching.maxInt(iMin,iMax);
        Assertions.assertEquals(iMax,result);
    }

    //Проверка результата - должено быть true при Utils#utilFunc2 true
    @Test
    public void ifElseExample_Test_True()
    {
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        MyBranching myBranching = new MyBranching(utilsMock);
        boolean result = myBranching.ifElseExample();
        Assertions.assertTrue(result);
    }

    //Проверка результата - должено быть false при Utils#utilFunc2 false
    @Test
    public void ifElseExample_Test_False()
    {
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        MyBranching myBranching = new MyBranching(utilsMock);
        boolean result = myBranching.ifElseExample();
        Assertions.assertFalse(result);
    }

    //Проверка выполнения Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 0
    //ожидаем 1 и 1  при Utils#utilFunc2 true
    @Test
    public void switchExample_Test_0_True()
    {
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        MyBranching myBranching = new MyBranching(utilsMock);
        myBranching.switchExample(0);
        Mockito.verify(utilsMock,Mockito.times(1)).utilFunc1("abc2");
        Mockito.verify(utilsMock,Mockito.times(1)).utilFunc2();
    }

    //Проверка выполнения Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 0
    //ожидаем 0 и 1 при Utils#utilFunc2 false
    @Test
    public void switchExample_Test_0_False()
    {
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        MyBranching myBranching = new MyBranching(utilsMock);
        myBranching.switchExample(0);
        Mockito.verify(utilsMock,Mockito.times(0)).utilFunc1("abc2");
        Mockito.verify(utilsMock,Mockito.times(1)).utilFunc2();
    }

    //Проверка выполнения Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 1
    //ожидаем 1 и 1
    @Test
    public void switchExample_Test_1()
    {
        Utils utilsMock = Mockito.mock(Utils.class);
        MyBranching myBranching = new MyBranching(utilsMock);
        myBranching.switchExample(1);
        Mockito.verify(utilsMock,Mockito.times(1)).utilFunc1("abc");
        Mockito.verify(utilsMock,Mockito.times(1)).utilFunc2();
    }

    //Проверка выполнения Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 2
    //ожидаем 0 и 1
    @Test
    public void switchExample_Test_2()
    {
        Utils utilsMock = Mockito.mock(Utils.class);
        MyBranching myBranching = new MyBranching(utilsMock);
        myBranching.switchExample(2);
        Mockito.verify(utilsMock,Mockito.times(0)).utilFunc1("abc");
        Mockito.verify(utilsMock,Mockito.times(1)).utilFunc2();
    }
}
